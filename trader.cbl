       IDENTIFICATION DIVISION.
       PROGRAM-ID FINAL.
       AUTHOR. WONGSATHORN.
       ENVIROMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT TRADE-FILE ASSIGN TO "teader0.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
              ORGANIZATION IS SEQUENTIAL
       DATA DIVISION.
       FILE SECTION.
       FD TRADER-REPORT-FILE. 
       01 PRINT-LINE            PIC X(44).
       FD TRADER-FILE
       01 TRADER-REC.
          88 END-OF-TRADER-FILE          VALUE HIGH-VALUES.
       05 TRADER-PROVINCE             PIC 9(2).
          05 TRADER-P INCOME          PIC 9(8).
          05 TRADER-MEMBER            PIC 9(2).
          05 TRADER-MEMBER INCOME     PIC 9(5).
       WORKING-STORAGE SECTION.
       01 PAGE-HEADER.
          05 FILTER                   PIC X(44) 
                                                VALUE
                  "TRADER REPORT"
       01 PAGE-FOOTING.
          05 FILLER                   PIC X(15) VALUE SPACES.
          05 FILLER                   PIC X(7)  VALUE.
       01 COLUMN-HEADING              PIC X(41)
                                                 VALUE
              "PROVINCE    P INCOME    MEMBER  MEMBER INCOME "
       TRADER-DETAIL-LINE
          05 FILLER                   PIC X    VALUE SPACES.
          05 PRN-PROVINCE             PIC 9(2).
          05 FILLER                   PIC X    VALUE SPACES.
          05 PRN-P INCOME             PIC 9(9).
          05 FILLER                   PIC X    VALUE SPACES.
          05 PRN-MEMBER               PIC 9(2).
          05 FILLER                   PIC X    VALUE SPACES.
          05 PRN-MEMBER INCOME        PIC 9(6).
       PROCEDURE DIVISION.
       PROCESS-TRADER-REPORT
           OPEN INPUT TRADER-FILE
           OPEN OUTPUT TRADER-REPORT-FILE
           PERFORM READ-TRADER-FILE
           PERFORM PROCESS-PAGE UNTIL ENF-OF-TRADER-FILE
           CLOSE TRADER-FIILE,TRADER-REPORT-FILE
           GOBLACK               
                                               



              
             
